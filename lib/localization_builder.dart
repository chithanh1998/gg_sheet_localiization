import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'localization_generator.dart';

/// A localization generator with build-runner.
Builder localizationGeneratorBuilder(BuilderOptions options) =>
    SharedPartBuilder([LocalizationGenerator()], 'm');
