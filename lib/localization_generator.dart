import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:csv/csv.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:path/path.dart' as path;
import 'package:source_gen/source_gen.dart';

import 'localization_model.dart';

const dotStr = ".";
const slashStr = "/";

/// A localization generator to create csv file from google sheet.
class LocalizationGenerator
    extends GeneratorForAnnotation<LocalizationGeneratorModel> {
  @override
  FutureOr<String> generateForAnnotatedElement(
          Element element, ConstantReader annotation, BuildStep buildStep) =>
      _generateSource(element, annotation);

  Future<String> _generateSource(
      Element element, ConstantReader annotation) async {
    try {
      final docId = annotation.read('docId').stringValue;
      final gId = annotation.read('gId').stringValue;
      final headers = {
        'Content-Type': 'text/csv; charset=utf-8',
        'Accept': '*/*'
      };
      final response = await http.get(
          Uri.parse(
              "https://docs.google.com/spreadsheets/export?format=csv&id=$docId&gid=$gId"),
          headers: headers);

      final classBuilder = StringBuffer();
      classBuilder.writeln(
          '// Generated at: ${_formatDateWithOffset(DateTime.now().toLocal())}');
      classBuilder.writeln('class ${element.displayName.substring(1)}{');

      if (response.statusCode == 200) {
        final List<String> ignoreFields = annotation
            .read('ignoreFields')
            .listValue
            .map((e) => e.toStringValue() ?? "")
            .toList();

        final bodyBytes = response.bodyBytes;

        final csvParser = _CSVParser(utf8.decode(bodyBytes));

        await _LocaleHandler(
          annotation: annotation,
          csvParser: csvParser,
          bodyBytes: bodyBytes,
        ).init();

        classBuilder.writeln(csvParser._supportedLocales(ignoreFields));
        // if (preservedKeywords.isNotEmpty) {
        //   classBuilder.writeln(csvParser._getLocaleKeys(preservedKeywords));
        // }
      } else {
        throw Exception('http reasonPhrase: ${response.reasonPhrase}');
      }
      classBuilder.writeln('}');
      return classBuilder.toString();
    } catch (e) {
      throw Exception(e);
    }
  }

  String _formatDateWithOffset(DateTime date,
      {String format = 'EEE, dd MMM yyyy HH:mm:ss'}) {
    String twoDigits(int n) => n >= 10 ? "$n" : "0$n";

    final hours = twoDigits(date.timeZoneOffset.inHours.abs());
    final minutes = twoDigits(date.timeZoneOffset.inMinutes.remainder(60));
    final sign = date.timeZoneOffset.inHours > 0 ? "+" : "-";
    final formattedDate = DateFormat(format).format(date);

    return "$formattedDate $sign$hours:$minutes";
  }
}

class _LocaleHandler {
  final ConstantReader annotation;
  final _CSVParser csvParser;
  final List<int> bodyBytes;

  late final docId;
  late final gId;
  late final outputDir;
  late final outputFileName;

  final current = Directory.current;

  _LocaleHandler({
    required this.csvParser,
    required this.annotation,
    required this.bodyBytes,
  });

  Future<String> init() async {
    outputDir = annotation.read('outDir').stringValue;
    outputFileName = annotation.read('outName').stringValue;

    docId = annotation.read('docId').stringValue;
    gId = annotation.read('gId').stringValue;

    _generateCSVFile();

    await _initLocalLanguage();

    return "LocaleHandler has been successfully completed";
  }

  void _generateCSVFile() {
    final Directory output = Directory.fromUri(Uri.parse(outputDir));
    final Directory outputPath =
        Directory(path.join(current.path, output.path, "$outputFileName.csv"));
    final File generatedFile = File(outputPath.path);

    if (!generatedFile.existsSync()) {
      generatedFile.createSync(recursive: true);
    }

    generatedFile.writeAsBytesSync(bodyBytes);
  }

  Future _initLocalLanguage() async {
    final List<dynamic> languages = csvParser.getLanguages();
    final Map<String, Map<String, String>> translations = {};
    final _ValuesForIgnoreFieldDTO _valuesForIgnoreFieldDTO =
        _ValuesForIgnoreFieldDTO(csvParser.lines);

    Future<Map<String, Map<String, String>>> _initLanguages() async {
      final List<String> ignoreFields = annotation
          .read('ignoreFields')
          .listValue
          .map((e) => e.toStringValue() ?? "")
          .toList();
      ignoreFields.forEach((ignoreField) {
        languages.remove(ignoreField);
      });

      _valuesForIgnoreFieldDTO.init(ignoreFields);

      languages
          .map((e) => {e.toString(): csvParser.mapByKey(e)})
          .forEach((element) => translations.addAll(element));

      if (translations.isEmpty) {
        throw Exception(
            "Error: translations is empty, please check Google doc file");
      }

      return translations;
    }

    await _initLanguages();

    Future _storeLanguage() async {
      await Future.forEach<MapEntry>(translations.entries, (e) async {
        String fileName = e.key.substring(0, 2);
        Map<String, String> value = e.value;
        value = value.dSort();

        // generate locale data dạng map > map
        Map<String, dynamic> gMap = _generateLocaleMap(value);

        _writeJsonFile(gMap, fileName);
      });
    }

    await _storeLanguage();
  }

  void _writeJsonFile(Map input, String fileName) {
    final Directory output = Directory.fromUri(Uri.parse(outputDir));
    var encoder = const JsonEncoder.withIndent("  ");

    // write to fileName
    final generatedJsonFile = File(
        Directory(path.join(current.path, output.path, "$fileName.json")).path);

    if (!generatedJsonFile.existsSync()) {
      generatedJsonFile.createSync(recursive: true);
    }

    generatedJsonFile.writeAsBytesSync(utf8.encode(encoder.convert(input)));
  }

  Map<String, dynamic> _generateLocaleMap(Map<String, dynamic> input) {
    // sắp xếp lại cho dễ đọc, dễ nhìn
    final sortedInput = input.dSort();

    Map<String, dynamic> output = {};

    sortedInput.forEach((sk, sv) {
      if (sk.contains(dotStr) || sk.contains(slashStr)) {
        List<String> keys = [];
        if (sk.contains(dotStr)) {
          keys = sk.split(dotStr);
        } else if (sk.contains(slashStr)) {
          keys = sk.split(slashStr);
        }

        if (keys.isNotEmpty) {
          Map cm = output;
          for (var i = 0; i < keys.length; i++) {
            final String k = keys[i];
            if (!cm.containsKey(k)) {
              final Map mm = output[k] ?? cm;
              if (i == (keys.length - 1)) {
                // thêm string vào map cuối cùng: ví dụ: a > b > c > {k: sv}
                cm.putIfAbsent(k, () => sv);
              } else {
                // chưa có key -> tạo map rỗng
                mm.putIfAbsent(k, () => {});
                cm = mm[k];
              }
            } else {
              // đã có key, next tới map kế tiếp
              if (cm[k] is Map) {
                cm = cm[k];
              }
            }
          }
        }
      } else {
        output.putIfAbsent(sk, () => sv);
      }
    });

    output = output.dSort();
    return output;
  }

  /*
    input là map sử dụng tại vscode dạng:
    {
      "a": {
        "b": {
          "c": {
            "d": "value"
          }
        }
      }
    }
    output: {"a.b.c.d:": "value"}
  */
}

class _CSVParser {
  final String fieldDelimiter;
  final String strings;
  final List<List<dynamic>> lines;

  _CSVParser(this.strings, {this.fieldDelimiter = ','})
      : lines = const CsvToListConverter()
            .convert(strings, fieldDelimiter: fieldDelimiter);

  List<dynamic> get _localesSupport => lines.first
      .sublist(1, lines.first.length)
      .where((value) => (value as String).isNotEmpty)
      .toList();

  // List<List<dynamic>> get _sheetWithLocaleLines => lines
  //     .map((childLine) => childLine.sublist(0, _localesSupport.length + 1))
  //     .toList();

  String _supportedLocales(List<String> ignoreFields) {
    final locales = _localesSupport;
    if (ignoreFields.isNotEmpty) {
      ignoreFields.forEach((e) {
        locales.remove(e);
      });
    }

    locales.map((currentLocale) {
      final languages = currentLocale.toString().split('_');

      if (languages.length == 1) {
        return "const Locale('${languages[0]}')";
      } else if (languages.length == 2) {
        return "const Locale('${languages[0]}', '${languages[1]}')";
      } else {
        throw Exception(
            "You are using wrong locale format. Please check again your wrong value ${currentLocale.toString()}. Correct format is languagesCode_countryCode : en_US");
      }
    }).toList();
    return 'static const supportedLocales = const [\n${locales.join(',\n')}\n];';
  }

  // String _getLocaleKeys(List<String> preservedKeywords) {
  //   final List<String> oldKeys = _sheetWithLocaleLines
  //       .getRange(1, _sheetWithLocaleLines.length)
  //       .map((e) => e.first.toString())
  //       .toList();

  //   final List<String> keys = [];
  //   final strBuilder = StringBuffer();
  //   oldKeys.forEach((element) {
  //     _reNewKeys(preservedKeywords, keys, element);
  //   });
  //   keys.sort();
  //   for (int index = 0; index < keys.length; index++) {
  //     final group1 = keys[index].split(RegExp(r"[._]"));
  //     if (index == 0) {
  //       _groupKey(strBuilder, group1, keys[index]);
  //       continue;
  //     }
  //     final group2 = keys[index - 1].split(RegExp(r"[._]"));
  //     if (group1.isEmpty || group2.isEmpty) {
  //       continue;
  //     }
  //     if (group1.first != group2.first) {
  //       strBuilder.writeln('\n   // ${group1.first}');
  //     }
  //     strBuilder
  //         .writeln('static const ${_joinKey(group1)} = \'${keys[index]}\';');
  //   }
  //   return strBuilder.toString();
  // }

  // void _groupKey(StringBuffer strBuilder, List<String> group, String key) {
  //   if (group.isEmpty) return;
  //   strBuilder.writeln('\n   // ${group.first}');
  //   strBuilder.writeln('static const ${_joinKey(group)} = \'$key\';');
  // }

  // void _reNewKeys(
  //     List<String> preservedKeywords, List<String> newKeys, String key) {
  //   final keys = key.split('.');
  //   for (int index = 0; index < keys.length; index++) {
  //     if (index == 0) {
  //       _addNewKey(newKeys, keys[index]);
  //       continue;
  //     }
  //     if (index == keys.length - 1 && preservedKeywords.contains(keys[index])) {
  //       continue;
  //     }
  //     _addNewKey(newKeys, keys.sublist(0, index + 1).join('.'));
  //   }
  // }

  // void _addNewKey(List<String> newKeys, String key) {
  //   if (!newKeys.contains(key)) {
  //     newKeys.add(key);
  //   }
  // }

  List getLanguages() {
    return lines.first.sublist(1, lines.first.length);
  }

  Map<String, String> mapByKey(String key) {
    final kIndex = lines.first.indexOf(key);

    final output = <String, String>{};
    for (var i = 1; i < lines.length; i++) {
      output.addAll({lines[i][0]: lines[i][kIndex]});
    }
    return output;
  }

  // String _capitalize(String str) =>
  //     '${str[0].toUpperCase()}${str.substring(1)}';

  // String _normalize(String str) => '${str[0].toLowerCase()}${str.substring(1)}';

  // String _joinKey(List<String> keys) =>
  //     _normalize(keys.map((e) => _capitalize(e)).toList().join());
}

class _ValuesForIgnoreFieldDTO {
  /*
    Lưu trữ của các fields ignore như comment, descriptionF
  */

  final List<List<dynamic>> lines;

  _ValuesForIgnoreFieldDTO(this.lines);

  final List<String> _keys = [];

  final Map<String, Map<String, String>> values = {};

  final Map<String, int> columnByKeys = {};

  List<int> _initKeysIndex(List<String> input) {
    _keys.addAll(input.where((e) {
      final int index = lines.first.indexOf(e);
      return index != -1;
    }));

    if (_keys.isEmpty) return [];

    return _keys.map((e) {
      final int index = lines.first.indexOf(e);
      columnByKeys.addAll({e: index});
      return index;
    }).toList();
  }

  void init(List<String> input) {
    final output = <String, Map<String, String>>{};

    final List<int> kIndexs = _initKeysIndex(input);

    _keys.map((e) {
      output.addAll({
        e: {},
      });

      return lines.first.indexOf(e);
    }).toList();

    for (var i = 1; i < lines.length; i++) {
      for (var j = 0; j < kIndexs.length; j++) {
        final int kIndex = kIndexs[j];
        final String v = lines[i][kIndex];
        if (v.isNotEmpty) {
          final String k = lines[i][0];

          output[_keys[j]]!.addAll({k: "${v}oldRow=${i + 1}"});
        }
      }
    }

    values.addAll(output);
  }
}

extension _MapExt<K, V> on Map<K, V> {
  Map<K, V> dSort() {
    return Map.fromEntries(this.entries.toList()
      ..sort((e1, e2) => e1.key.toString().compareTo(e2.key.toString())));
  }

  /*
    Merge map:
    - Output: 
      - 1 map chứa toàn bộ key-value của 2 input map.
      - Nếu 2 input map trùng keys -> lấy value ở map 1.
  */
}
