class LocalizationGeneratorModel {
  final String docId;
  final String gId;
  final int version;
  final String
      outDir; // output directory for language.json and google spreadSheet file name
  final String outName; // output google spreadSheet file name
  final List<String> ignoreFields;

  const LocalizationGeneratorModel({
    required this.docId,
    this.gId = "",
    this.version = 1,
    this.outDir = 'assets/locales',
    this.outName = 'gg_sheet_localiization',
    this.ignoreFields = const [],
  });
}
